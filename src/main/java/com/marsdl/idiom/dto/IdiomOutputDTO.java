package com.marsdl.idiom.dto;

public class IdiomOutputDTO {

    String message;
    String rawText;
    String uuid;
    String isBingo;
    String renderTts;
    String wordVal;
    String wordNumVal;
    String percentVal;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRawText() {
        return rawText;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIsBingo() {
        return isBingo;
    }

    public void setIsBingo(String isBingo) {
        this.isBingo = isBingo;
    }

    public String getRenderTts() {
        return renderTts;
    }

    public void setRenderTts(String renderTts) {
        this.renderTts = renderTts;
    }

    public String getWordVal() {
        return wordVal;
    }

    public void setWordVal(String wordVal) {
        this.wordVal = wordVal;
    }

    public String getWordNumVal() {
        return wordNumVal;
    }

    public void setWordNumVal(String wordNumVal) {
        this.wordNumVal = wordNumVal;
    }

    public String getPercentVal() {
        return percentVal;
    }

    public void setPercentVal(String percentVal) {
        this.percentVal = percentVal;
    }
}
