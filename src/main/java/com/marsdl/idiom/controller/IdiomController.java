package com.marsdl.idiom.controller;

import com.marsdl.idiom.dto.WebResult;
import com.marsdl.idiom.service.IdiomChain;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class IdiomController {

    @Autowired
    IdiomChain idiomChain;

    @RequestMapping(value = "idiom")
    public WebResult idiom(String words, String uuid) {
        if(StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString();
        }
        return idiomChain.idiomReq(words, uuid);
    }

}
