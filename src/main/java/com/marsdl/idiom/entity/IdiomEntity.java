package com.marsdl.idiom.entity;

import java.util.List;

public class IdiomEntity {

    int totalCount;
    String words;
    List<String> wordsPyin;
    long currentTime;
    String uuid;
    boolean isBingo = true;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public List<String> getWordsPyin() {
        return wordsPyin;
    }

    public void setWordsPyin(List<String> wordsPyin) {
        this.wordsPyin = wordsPyin;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isBingo() {
        return isBingo;
    }

    public void setBingo(boolean bingo) {
        isBingo = bingo;
    }
}
