# Idiom

#### 介绍
成语接龙应用的SDK，成语库采用二进制文件存储。并且暴露对应的配置接口，可以对其中的成语进行增删改查。 <br/>

#### 使用方式
> clone下代码后对代码进行打包

`mvn clean package` <br/>
` java -jar Idiom-0.0.1-SNAPSHOT.war`

##### 开始成语接龙
url :
http://localhost:8080/idiom/idiom?words=%E6%88%90%E8%AF%AD%E6%8E%A5%E9%BE%99&uuid=d5c44333-a22f-4950-82f4-157a7637b87d
type: get

返回结果
```json
{
    "code": "",
    "msg": "success",
    "data": {
        "message": "success",
        "rawText": "成语接龙",
        "uuid": "d5c44333-a22f-4950-82f4-157a7637b87d",
        "isBingo": "true",
        "renderTts": "成语接龙开始啦，接龙成语要首尾同音，不想玩了随时和我说不玩了。接受挑战吧 瓜熟蒂落，开始吧，皮卡丘！",
        "wordVal": "瓜熟蒂落",
        "wordNumVal": "1",
        "percentVal": "0"
    }
}
```
##### 更换成语接龙
url: http://localhost:8080/idiom/idiom?words=%E6%8D%A2%E4%B8%80%E4%B8%AA&uuid=d5c44333-a22f-4950-82f4-157a7637b87d

```json
{
    "code": "",
    "msg": "success",
    "data": {
        "message": "success",
        "rawText": "换一个",
        "uuid": "d5c44333-a22f-4950-82f4-157a7637b87d",
        "isBingo": "true",
        "renderTts": "那好啦，我们换一个，这次我出的成语是。月书赤绳，您继续吧。",
        "wordVal": "月书赤绳",
        "wordNumVal": "1",
        "percentVal": "0"
    }
}
```

##### 结束成语接龙

url: http://localhost:8080/idiom/idiom?words=%E4%B8%8D%E7%8E%A9%E4%BA%86&uuid=d5c44333-a22f-4950-82f4-157a7637b87d

```json
{
    "code": "",
    "msg": "success",
    "data": {
        "message": "success",
        "rawText": "不玩了",
        "uuid": null,
        "isBingo": "true",
        "renderTts": "游戏结束啦！看看你的成绩吧。你共进行了1次接龙，打败了全国 0% 的人。想玩成语接龙，随时找我哦。",
        "wordVal": null,
        "wordNumVal": "1",
        "percentVal": "0"
    }
}
```

#### 成语接龙业务处理的框架图 
![单个业务处理](doc/img/service_single.png)